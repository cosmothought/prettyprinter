{
  description = "prettyprinter";

  inputs.haskellNix.url = "github:input-output-hk/haskell.nix";
  inputs.nixpkgs.follows = "haskellNix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils, haskellNix }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        componentOpts = {
          doCoverage = true;
        };
        overlays = [
          haskellNix.overlay
          (final: prev: {
            prettyprinterProject =
              final.haskell-nix.project'
                {
                  src = ./.;
                  index-state = "2023-01-14T00:00:00Z";
                  compiler-nix-name = "ghc925";
                  # This is used by `nix develop .` to open a shell for use with
                  # `cabal`, `hlint` and `haskell-language-server`
                  shell.tools = {
                    cabal = "latest";
                    hlint = "latest";
                    alex = "latest";
                    haskell-language-server = "latest";
                  };
                  modules = [
                  ];
                  # Non-Haskell shell tools go here
                  shell.buildInputs = with pkgs; [
                    nixpkgs-fmt
                    pkg-config
                    zlib
                  ];
                  # This adds `js-unknown-ghcjs-cabal` to the shell.
                  # shell.crossPlatforms = p: [p.ghcjs];
                };
          })
        ];
        pkgs = import nixpkgs { inherit system overlays; inherit (haskellNix) config; };
        flake = pkgs.prettyprinterProject.flake {
          # This adds support for `nix build .#js-unknown-ghcjs:hello:exe:hello`
          # crossPlatforms = p: [p.ghcjs];
        };
        myShell = pkgs.prettyprinterProject.shellFor {
          tools = { cabal = "latest"; hlint = "latest"; haskell-language-server = "latest"; cabal-install = "latest"; };
        };
      in
      {
        inherit (flake); #flake // {
        # Built by `nix build .`
        packages = flake.packages // {
          shell = myShell;

          default = flake.packages."prettyprinter-ansi-terminal:test:prettyprinter-ansi-terminal-test";

        };
      });
}
